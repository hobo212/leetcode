/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.array;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author hobo
 * @description 数组中是否包含重复元素
 * @email hobo@bubi.cn
 * @creatTime 2018/1/23 10:00
 * @since 1.0.0
 */
public class ContainsDuplicateTest {

    @Test
    public void testContainsDuplicate(){
        int[] array = new int[]{2,14,18,22,22};
        System.out.println(Arrays.toString(array));
        System.out.println("contains duplicate ?:" + containsDuplicate(array));
        System.out.println("contains duplicate ?:" + containsDuplicate_HashTable(array));
    }
    public boolean containsDuplicate_HashTable(int[] nums) {
        if (nums == null || nums.length == 0) {
            return  false;
        }
        Node[] nodes = new Node[nums.length];
        for (int num : nums) {
            int index = Math.abs(num) % nums.length;
            Node newNode = new Node(num);
            Node oldNode = nodes[index];
            if (oldNode == null) {
                nodes[index] = newNode;
            } else {
                Node lastNode  = null;
                while(true) {
                    if (oldNode == null) {
                        break;
                    }
                    if (oldNode.key == num) {
                        return true;
                    }
                    lastNode = oldNode;
                    oldNode = oldNode.next;
                }
                lastNode.next = newNode;
            }
        }
        return false;
    }


    public boolean containsDuplicate(int[] nums) {
        if (nums == null || nums.length == 1) {
            return false;
        }
        for(int i = 0; i < nums.length; i ++) {
            for(int j = i+1; j< nums.length; j++) {
                if (nums[i] == nums[j]){
                    return true;
                }
            }
        }
        return false;
    }
    class Node{
        public int key;
        public Node next;
        Node(int key){
            this.key = key;
        }
    }
}
