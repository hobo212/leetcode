/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.array;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author hobo
 * @description 字符串最右前移
 * @email hobo@bubi.cn
 * @creatTime 2018/1/22 16:18
 * @since 1.0.0
 */
public class RotateArrayTest {

    @Test
    public void testRotateArray() {
        int[] array = new int[]{1, 2};
        System.out.println("original array is:" + Arrays.toString(array));
        rotate(array, 3);
        System.out.println("after rotate the array is:" + Arrays.toString(array));
    }

    public void reverse(int[] array,int from ,int end){
        for (int i = 0; i < array.length/2 ; i++) {
            array[from] = array[end];
        }
    }

    /**
     * Rotate an array of n elements to the right by k steps.
     * <p>
     * For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
     * <p>
     * Note:
     * Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
     *
     * @param nums
     * @param k
     */
    public void rotate(int[] nums, int k) {
        if (nums == null || nums.length == 1) {
            return;
        }
        //全部向前移动
        k = k % nums.length;
        if ( k == 0) {
            return;
        }
        for (int i = 0; i < k; i++) {
            int tmp = nums[nums.length - 1];
            for (int j = nums.length - 1; j > 0; j--) {
                nums[j] = nums[j - 1];
            }
            nums[0] = tmp;
        }
    }
}
