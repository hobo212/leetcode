/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.array;

import org.junit.Test;

/**
 * @author hobo
 * @description 寻找最好收益
 * @email hobo@bubi.cn
 * @creatTime 2018/1/22 14:40
 * @since 1.0.0
 */
public class BestTime2BuySellStockTest {

    @Test
    public void testBestTime2BuySellStockI() {
        int[] array = new int[]{102, 45, 88, 99, 20, 180, 26, 72};
        System.out.println("one buy one sell,best profit is :" + bestTime2BuySellStockI(array));    }

    /**
     * Say you have an array for which the ith element is the price of a given stock on day i.
     If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
     *先买后卖
     * @param prices
     * @return
     */
    public int bestTime2BuySellStockI(int[] prices) {
        if (prices == null || prices.length < 2) {
            throw new IllegalArgumentException("prices数组长度不能小于2");
        }
        int last = 0;
        for (int i = 0; i < prices.length -1; i++) {
            for (int j = i; j < prices.length; j ++) {
                last = Math.max(prices[i] - prices[j], last);
            }
        }
        return last;
    }

    @Test
    public void testBestTime2BuySellStockII() {
        int[] array = new int[]{102, 45, 88, 99, 20, 180, 26, 72};
        System.out.println("no times limit,best profit is :" + bestTime2BuySellStockII(array));
    }

    public int bestTime2BuySellStockII(int[] prices){
        if (prices == null || prices.length < 2) {
            return 0;
        }
        int profit = 0;
        for (int i = 1; i < prices.length; i++) {
            int profitTmp = prices[i] - prices[i-1];
            if (profitTmp> 0){
                profit += profitTmp;
            }
        }
        return profit;
    }
}
