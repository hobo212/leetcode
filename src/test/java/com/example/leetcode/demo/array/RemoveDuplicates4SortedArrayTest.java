/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.array;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author hobo
 * @description 移除已排序数组中的重复元素
 * @email hobo@bubi.cn
 * @creatTime 2018/1/22 14:20
 * @since 1.0.0
 */
public class RemoveDuplicates4SortedArrayTest {
    /**
     * Given a sorted array, remove the duplicates in-place such that each element appear only once and return the new length.

     Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

     Example:

     Given nums = [1,1,2],

     Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
     It doesn't matter what you leave beyond the new length.
     */
    @Test
    public void testRemoveDuplicates4SortedArray(){
        //测试数据
        int[] array = new int[]{1, 1, 2};
        int size = removeDuplicates4SortedArray(array);
        //打印测试结果
        array = Arrays.copyOf(array,size);
        System.out.println("array size is :" + size);
        System.out.println(Arrays.toString(array));
    }

    /**
     *
     * @param array
     * @return
     */
    public int removeDuplicates4SortedArray(int[] array){
        if (array == null || array.length == 0) {
            return 0;
        }
        if (array.length == 1) {
            return 1;
        }

        int size = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[size] != array[i] ) {
                array[++size] = array[i];
            }
        }
        return size + 1;

    }


}
