/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.array;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/1/25 17:52
 * @since 1.0.0
 */
public class IntersectionofTwoArraysTest {

    public void testIntersectionofTwoArrays() {

    }

    public int[] intersectionofTwoArrays(int[] nums1, int[] nums2) {
        int[] result = new int[0];
        if (nums1 == null || nums1.length == 0 || nums2 == null || nums2.length == 0) {
            return result;
        }
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    int[] tmp = new int[result.length+1];
                    tmp[result.length] = nums1[i];
                    System.arraycopy(result,0,tmp,0,result.length);
                    result = tmp;
                    break;
                }

            }

        }
        return result;
    }
}
