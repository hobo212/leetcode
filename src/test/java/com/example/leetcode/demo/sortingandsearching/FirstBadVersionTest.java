/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.sortingandsearching;

import org.junit.Test;

/**
 * @author hobo
 * @description 查找最早的失败版本
 * @email hobo@bubi.cn
 * @creatTime 2018/2/2 14:57
 * @since 1.0.0
 */
public class FirstBadVersionTest {

    @Test
    public void test(){
        firstBadVersion(18);
    }

    /**
     * You are a product manager and currently leading a team to develop a new product. Unfortunately, the latest version of your product fails the quality check. Since each version is developed based on the previous version, all the versions after a bad version are also bad.

     Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.

     You are given an API bool isBadVersion(version) which will return whether version is bad. Implement a function to find the first bad version. You should minimize the number of calls to the API.

     Credits:
     Special thanks to @jianchao.li.fighter for adding this problem and creating all test cases.
     * @param n
     * @return
     */
    public int firstBadVersion(int n) {
        int last = n;
        while (true) {
            if (isBadVersion(n)) {
                last = n;
                n = n/2;
                if (last == n ) {
                    break;
                }
            }else{
                if (last == n|| n == last -1){
                    break;
                }
                n = n + (last - n)/2;
            }
        }
        return last;
    }
    public int firstBadVersion2(int n) {
        int start = 1;
        int end  = n;
        while (start + 1 <  n) {
            int mid = start + (end - start)/2;
            if (isBadVersion(mid)) {
                end = mid;
            }else {
                start = mid;
            }
        }
        if (isBadVersion(start)) {
            return start;
        }
        return end;
    }
    public boolean isBadVersion(int n) {
        if (n >= 7) {
            return true;
        }
        return false;
    }
}
