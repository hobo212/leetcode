/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.sortingandsearching;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author hobo
 * @description 排序算法测试
 * @email hobo@bubi.cn
 * @creatTime 2018/2/24 18:17
 * @since 1.0.0
 */
public class SortTest {

    @Test
    public void test() {
        int[] array = new int[]{100, 88, 0, 11, 3, 66, 3, 9, 77};
        System.out.println(Arrays.toString(array));
        mergeSort(array);
        System.out.println("bubble sort:" + Arrays.toString(array));
    }

    /**
     * 冒泡排序 O(n)-O(n^2) 稳定
     *
     * @param array
     */
    public void bubbleSort(int[] array) {
        if (array == null || array.length < 2) {
            return;
        }
        for (int i = 0; i < array.length; i++) {
            boolean isSwag = false;
            for (int j = i + 1; j < array.length; j++) {
                int tmpB = array[j];
                if (array[i] > tmpB) {
                    isSwag = true;
                    array[j] = array[i];
                    array[i] = tmpB;
                }
            }
            if (!isSwag) {
                break;
            }
        }
    }

    /**
     * 插入排序 O(n)-O(n^2) 稳定
     * 1.取出第一个元素，当做已排序的序列
     * 2.依次向后取一个元素，然后与已排序的序列从后往前依次比较
     * 3.如果被扫描的元素（已排序）大于新元素，将该元素后移一位
     * 4.重复步骤3，直到找到已排序的元素小于或者等于新元素的位置
     * 5.将新元素插入到该位置
     * 6.重复步骤2-5
     *
     * @param array
     */
    public void insertionSort(int[] array) {
        if (array == null || array.length < 2) {
            return;
        }
        for (int i = 1; i < array.length; i++) {
            int value = array[i];
            //空出的元素位置
            int index = i;
            for (int j = i - 1; j >= 0; j--) {
                if (array[j] > value) {
                    //元素后移
                    array[j + 1] = array[j];
                    //空出的元素位置
                    index = j;
                } else {
                    break;
                }
            }
            array[index] = value;
        }
    }

    /**
     * 快速排序 O(nlogn) 不稳定
     * 1.从数列中挑出一个元素作为基准数。
     * 2.分区过程，将比基准数大的放到右边，小于或等于它的数都放到左边。
     * 3.再对左右区间递归执行第二步，直至各区间只有一个数。
     *
     * @param array
     */
    public void quickSort(int[] array) {
        if (array == null || array.length < 2) {
            return;
        }
        quickSort(array, 0, array.length - 1);
    }

    private void quickSort(int[] array, int left, int right) {
        if (array == null || array.length < 2) {
            return;
        }
        if (left >= right) {
            return;
        }
        int i = left;
        int j = right;
        int key = array[left];
        //移动挖坑填坑，先填充i,在填充j，最后i=j时还剩下一个坑
        while (i < j) {
            while (i < j && array[j] >= key) {
                j--;
            }
            if (i < j) {
                array[i++] = array[j];
            }
            while (i < j && array[i] < key) {
                i++;
            }
            if (i < j) {
                array[j--] = array[i];
            }
        }
        array[i] = key;
        quickSort(array, left, i - 1);
        quickSort(array, i + 1, right);
    }

    /**
     * 合并排序 O(nlogn) - O(nlogn) 稳定
     * 1.将原序列不断二分直至剩下一个元素
     * 2.将步骤一种的元素递归合并排序相邻二个小组成一个序列
     *
     * @param array
     */
    private void mergeSort(int[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    //合并
    private void merge(int[] array, int left, int mid, int right) {
        int[] temp = new int[right - left + 1];
        int i = left;
        int j = mid + 1;
        int k = 0;
        while (i <= mid && j <= right) {
            if (array[i] < array[j]) {
                temp[k++] = array[i++];
            } else {
                temp[k++] = array[j++];
            }
        }
        while (i <= mid) {
            temp[k++] = array[i++];
        }
        while (j <= right) {
            temp[k++] = array[j++];
        }
        for (int l = 0; l < temp.length; l++) {
            array[left + l] = temp[l];
        }
    }

    //二路递归调用（退出条件为左右指针碰撞）
    private void mergeSort(int[] array, int left, int right) {
        int mid = (left + right) / 2;
        if (left >= right) {
            return;
        }
        mergeSort(array, left, mid);
        mergeSort(array, mid + 1, right);
        merge(array, left, mid, right);
    }


}
