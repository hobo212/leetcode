/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.sortingandsearching;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author hobo
 * @description 已排序数组合并
 * @email hobo@bubi.cn
 * @creatTime 2018/2/1 15:44
 * @since 1.0.0
 */
public class MergeSortedArrayTest {
    @Test
    public void test() {
        int[] nums1 = new int[20];
        nums1[0] = 0;
        int[] nums2 = new int[]{2};
        System.out.println("array 1 is: " + Arrays.toString(nums1));
        System.out.println("array 2 is: " + Arrays.toString(nums2));
        merge(nums1, 0, nums2, 1);
        System.out.println("merged is :" + Arrays.toString(nums1));
    }

    /**
     * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
     * <p>
     * Note:
     * You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.
     *
     * @param nums1
     * @param m
     * @param nums2
     * @param n
     */
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (nums1 == null || nums2 == null) {
            return;
        }
        if (nums1.length < m + n) {
            return;
        }
        int i = m-1;
        int j = n-1;
        int z = m+n -1;
        while(i >=0 && j >=0 ) {
            if (nums1[i] >= nums2[j]) {
                nums1[z--] = nums1[i--];
            }else{
                nums1[z--] = nums2[j--];
            }
        }
        while (i >= 0) {
            nums1[z--] = nums1[i--];
        }
        while (j >= 0) {
            nums1[z--] = nums2[j--];
        }
    }
    @Test
    public void testMoveNext(){
        int[] nums = new int[6];
        nums[0] = 1;
        nums[1] = 2;
        nums[2] = 3;
        System.out.println(Arrays.toString(nums));
        moveNext(nums,3,0,1);
        System.out.println(Arrays.toString(nums));
    }
    /**
     * nums工n个元素中从index后移动一个位置
     * @param nums
     * @param n
     */
    private void moveNext(int[] nums,int n, int index, int step){
        for (int i = 0; i < n - index; i++) {
            nums[n - i - 1 + step] = nums[n - i -1];
        }
    }
}
