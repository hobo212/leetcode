/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.linkedlist;

/**
 * @author hobo
 * @description 链表元素
 * @email hobo@bubi.cn
 * @creatTime 2018/1/31 18:27
 * @since 1.0.0
 */
public class ListNode {
    public int val;
    public ListNode next;

    ListNode(int x) {
        val = x;
    }

    public ListNode getNext() {
        return next;
    }

    public ListNode setNext(ListNode next) {
        this.next = next;
        return this;
    }

    public static ListNode getNode(int val) {
        return new ListNode(val);
    }


}
