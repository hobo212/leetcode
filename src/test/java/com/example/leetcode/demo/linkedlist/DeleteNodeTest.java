/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.linkedlist;

import org.junit.Test;

import java.util.Arrays;

/**
 * @author hobo
 * @description 测试删除链表中的一个元素
 * @email hobo@bubi.cn
 * @creatTime 2018/1/31 18:26
 * @since 1.0.0
 */
public class DeleteNodeTest {
    private ListNode head;
    private ListNode tail;

    @Test
    public void test() {
        ListNode[] list = new ListNode[5];
        list[4] = ListNode.getNode(664);
        list[3] = ListNode.getNode(663).setNext(list[4]);
        list[2] = ListNode.getNode(662).setNext(list[3]);
        list[1] = ListNode.getNode(661).setNext(list[2]);
        list[0] = ListNode.getNode(660).setNext(list[1]);
        head = list[0];
        tail = list[4];
        deleteNode(list[2]);
        deleteNode(list[0]);
        System.out.println(Arrays.toString(list));
        ListNode now = head;
        while (true) {
            if (now == null) {
                break;
            }
            System.out.println(now.val);
            now = now.next;
        }
    }

    /**
     *Write a function to delete a node (except the tail) in a singly linked list, given only access to that node.

     Supposed the linked list is 1 -> 2 -> 3 -> 4 and you are given the third node with value 3, the linked list should become 1 -> 2 -> 4 after calling your function.
     删除第一个，删除最后一个，删除中间的
     * @param node
     */
    public void deleteNode(ListNode node) {
        ListNode now = head;
        ListNode pre = null;
        while (now != null) {
            if (node == now) {
                if (pre != null) {
                    pre.next = now.next;
                }
                if (now == head ) {
                    head = now.next;
                }
                now = null;
            } else {
                pre = now;
                now = now.next;
            }
        }
    }
}
