/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.dynamic;

import org.junit.Test;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/2/2 16:18
 * @since 1.0.0
 */
public class BestTime2BuySellStockTest {

    @Test
    public void test(){
        int[] array = new int[]{7, 10, 5, 3, 6,1, 9};
        System.out.println(maxProfit(array));

        int[] array2 = new int[]{7, 6, 4, 3, 1};
        System.out.println(maxProfit(array2));
    }

    /**
     * Say you have an array for which the ith element is the price of a given stock on day i.

     If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
     * @param prices
     * @return
     */
    public int maxProfit(int[] prices) {
        int max = 0;
        int lastP = prices[0];
        for (int i = 1; i < prices.length; i++) {
            int tmp = prices[i];
            if (tmp < lastP) {
                lastP = tmp;
            } else {
                max = Math.max(max, tmp - lastP);
            }
        }
        return max;
    }
}
