/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description 字符串查找
 * @email hobo@bubi.cn
 * @creatTime 2018/1/30 15:30
 * @since 1.0.0
 */
public class StrSearchTest {
    @Test
    public void test() {
        String a = "bba";
        String b = "a";
        strStr2(a, b);
    }

    /**
     * 依次遍历显示回退法
     * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
     *
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr(String haystack, String needle) {
        if (haystack == null) {
            return -1;
        }
        if (needle == null) {
            return -1;
        }
        if ("".equals(needle)) {
            return 0;
        }
        if (needle.length() > haystack.length()) {
            return -1;
        }
        int i = 0;
        int j = 0;
        while (i < haystack.length() && j < needle.length()) {
            if (haystack.charAt(i) == needle.charAt(j)) {
                i++;
                j++;
                if (j == needle.length()) {
                    return i - j;
                }
            } else {
                i = i - j + 1;
                j = 0;
            }
        }
        return -1;
    }

    /**
     * KMP
     * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
     *
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr2(String haystack, String needle) {
        if (haystack == null) {
            return -1;
        }
        if (needle == null) {
            return -1;
        }
        if ("".equals(needle)) {
            return 0;
        }
        if (needle.length() > haystack.length()) {
            return -1;
        }
        int i = 0;
        int j = 0;
        int[] array = getLongestCommonPrefixArray(needle);
        while (i < haystack.length() && j < needle.length()) {
            if (haystack.charAt(i) == needle.charAt(j)) {
                i++;
                j++;
                if (j == needle.length()) {
                    return i - j;
                }
            } else {
                i = i - array[j] + 1;
                j = 0;
            }
        }
        return -1;
    }
    private int[] getLongestCommonPrefixArray(String text){
        int[] result = new int[text.length()];
        for (int i = 0; i < text.length(); i++) {
            result[i] = getLongestCommonPrefix(text.substring(0,i+1));
        }
        return result;
    }


    //获取前缀和后缀的最长共有元素
    private int getLongestCommonPrefix(String text){
        String[] preArray = new String[text.length()];
        String[] lastArray =  new String[text.length()];
        for (int i = 0; i < text.length()-1; i++) {
            preArray[i] = text.substring(0,i+1);
            lastArray[i] = text.substring(text.length()-i,text.length());
        }
        int maxLength = 0;
        for (int i = 0; i < preArray.length; i++) {
            for (int j = 0; j < lastArray.length; j++) {
                if (preArray[i] == lastArray[j]) {
                    maxLength = preArray[i] == null ? 0 : preArray[i].length();
                }
            }
        }
        return maxLength;
    }
    /**
     * BM
     * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
     *
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr3(String haystack, String needle) {
        return -1;
    }
}
