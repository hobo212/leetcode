/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description 反转32bits的整数（注意溢出的情况）
 * @email hobo@bubi.cn
 * @creatTime 2018/1/29 10:06
 * @since 1.0.0
 */
public class ReverseIntegerTest {

    @Test
    public void test() {
        int testInt = -2147483648;
        int result = reverse(testInt);
        System.out.println(result);
    }

    /**
     * Given a 32-bit signed integer, reverse digits of an integer
     * @param x
     * @return
     */
    public int reverse(int x) {
        int result = 0;
        while (true) {
            if (Integer.MAX_VALUE / 10 < result) {
                return  0;
            }
            if (Integer.MIN_VALUE/10 > result) {
                return 0;
            }
            result = result * 10;
            //取余数得到最后一位
            int last = x % 10;
            result += last;
            x = x / 10;
            if (x == 0) {
                break;
            }
        }
        return result;
    }

}
