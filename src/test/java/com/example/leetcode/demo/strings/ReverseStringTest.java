/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description 字符串反转
 * @email hobo@bubi.cn
 * @creatTime 2018/1/29 9:51
 * @since 1.0.0
 */
public class ReverseStringTest {

    @Test
    public void test() {
        String testStr = "123456789abcd";
        String result = reverseString(testStr);
        System.out.println("reverse string '"+testStr+"'");
        System.out.println(result);
    }

    /**
     * Write a function that takes a string as input and returns the string reversed.
     Example:
     Given s = "hello", return "olleh".
     * @param s
     * @return
     */
    public String reverseString(String s) {
        if (s == null || "".equals(s.toString()) || s.length() == 1) {
            return s;
        }
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length / 2; i++) {
            char tmp = chars[i];
            chars[i] = chars[chars.length - 1 -i];
            chars[chars.length - 1 -i] = tmp;
        }
        return  new String(chars);
    }
}
