/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/1/30 12:01
 * @since 1.0.0
 */
public class CountAndSayTest {
    @Test
    public void test() {
        String result = countAndSay(4);
        System.out.println(result);
    }

    public String countAndSay(int n) {
        if (n < 1) {
            return "";
        }
        String result = "1";
        while (--n > 0) {
            String tmp = "";
            int k = 0;
            char last = result.charAt(0);
            int count = 0;
            while (true) {
                //k个last
                if (result.charAt(k) == last) {
                    count++;
                } else {
                    //count个last
                    tmp = tmp + count + last;
                    last = result.charAt(k);
                    count = 1;
                }
                k++;
                if (k >= result.length()) {
                    //count个last
                    tmp = tmp + count + last;
                    break;
                }

            }
            result = tmp;
        }
        return result;
    }
}
