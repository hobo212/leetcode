/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/1/30 10:59
 * @since 1.0.0
 */
public class LongestCommonPrefixTest {
    @Test
    public void test(){
        String[] strs = new String[]{"c","d"};
        String result = longestCommonPrefix(strs);
        System.out.println(result);
    }

    /**
     * Write a function to find the longest common prefix string amongst an array of strings.
     * @param strs
     * @return
     */
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        if(strs.length == 1) {
            return strs[0];
        }
        String result = null;
        for (int i = 0; i < strs.length; i++) {
            String strA = strs[i];
            for (int j = i+1; j < strs.length; j++) {
                String strB = strs[j];
                int k = 0;
                while(k < Math.min(strA.length(),strB.length()) && strA.charAt(k) == strB.charAt(k)){
                    k++;
                }
                if (result == null ) {
                    result = strA.substring(0,k);
                }else if (k < result.length()) {
                    result = strA.substring(0,k);
                }

            }
        }
        return result;
    }

}
