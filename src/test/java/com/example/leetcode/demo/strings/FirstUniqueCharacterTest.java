/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/1/29 11:32
 * @since 1.0.0
 */
public class FirstUniqueCharacterTest {

    @Test
    public void test(){
        String s = "cc";
        int result = firstUniqChar(s);
        System.out.println(s + ",first Uniq Char index is:" +result);

    }

    public int firstUniqChar(String s) {
        if (s == null || "".equals(s.trim())) {
            return -1;
        }
        if (s.length() == 1) {
            return 0;
        }
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char tmp = chars[i];
            boolean contains = false;
            for (int j = 0; j < chars.length; j++) {
                if (tmp == chars[j] && i != j) {
                    contains = true;
                    break;
                }
            }
            if (!contains) {
                return i;
            }
        }
        return -1;
    }
}
