/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/1/29 13:12
 * @since 1.0.0
 */
public class ValidAnagramTest {
    @Test
    public void test(){
        String s = "aacc";
        String t = "ccac";
        boolean result = isAnagram(s,t);
        System.out.println(result);
    }

    /**
     * Given two strings s and t, write a function to determine if t is an anagram of s.

     For example,
     s = "anagram", t = "nagaram", return true.
     s = "rat", t = "car", return false.
     * @param s
     * @param t
     * @return
     */
    public boolean isAnagram(String s, String t) {
        if (s == null || t == null) {
            return false;
        }
        if (s.equals(t) ) {
            return true;
        }
        if (s.length() != t.length()) {
            return false;
        }
        HashMap<String,Integer> hashMap  = init(s);
        HashMap<String,Integer> hashMap2  = init(t);
        if (hashMap.size() != hashMap2.size()) {
            return false;
        }
        for (Map.Entry<String, Integer> stringIntegerEntry : hashMap.entrySet()) {
            String keyTmp = stringIntegerEntry.getKey();
            if (!hashMap2.containsKey(keyTmp)) {
                return false;
            }
            if (!stringIntegerEntry.getValue().equals(hashMap2.get(keyTmp))) {
                return false;
            }
        }
        return true;
    }
    private  HashMap<String,Integer> init(String s){
        HashMap<String,Integer> hashMap  = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            String tmp = String.valueOf(s.charAt(i));
            Integer times= hashMap.get(tmp);
            if (times == null) {
                times = 0;
            }
            hashMap.put(tmp,++times);
        }
        return hashMap;
    }

}
