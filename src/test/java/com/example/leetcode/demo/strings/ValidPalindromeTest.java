/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.leetcode.demo.strings;

import org.junit.Test;

/**
 * @author hobo
 * @description 验证是否满足回文结构
 * @email hobo@bubi.cn
 * @creatTime 2018/1/29 15:08
 * @since 1.0.0
 */
public class ValidPalindromeTest {

    @Test
    public void test() {
        String s = "a.";
        boolean result = isPalindrome(s);
        System.out.println(result);
    }

    /**
     * Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
     * <p>
     * For example,
     * "A man, a plan, a canal: Panama" is a palindrome.
     * "race a car" is not a palindrome.
     *
     * @param s
     * @return
     */
    public boolean isPalindrome(String s) {
        if (s == null) {
            return false;
        }
        if (s.length() == 1) {
            return true;
        }
        int i = 0;
        int j = s.length() - 1;
       while (i < j && j >=0 && i < s.length()) {
           if (!isLetter(s.charAt(i))) {
               i++;
               continue;
           }
           if (!isLetter(s.charAt(j))) {
               j--;
               continue;
           }
           if (!isSame(s.charAt(i),s.charAt(j))) {
               return false;
           }
           i++;
           j--;
       }
       return true;
    }

    public boolean isLetter(char charTmp) {
        return (charTmp >= 'A' && charTmp <= 'Z') || (charTmp >= 'a' && charTmp <= 'z') || (charTmp > '0' && charTmp < '9');
    }

    public boolean isSame(char charTmpA, char charTmpB) {
        if(charTmpA >= 'A' && charTmpA <= 'Z')
            charTmpA = (char)(charTmpA + ('a' + 'A'));
        if(charTmpB >= 'A' && charTmpB <= 'Z')
            charTmpB = (char)(charTmpB + ('a' + 'A'));
        return charTmpA == charTmpB;
    }
}
