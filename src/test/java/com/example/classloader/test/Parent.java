/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.classloader.test;

/**
 * @author hobo
 * @description 父类
 * @email hobo@bubi.cn
 * @creatTime 2018/2/24 10:01
 * @since 1.0.0
 */
public class Parent {

    public static User user = new User("parent");

    static {
        System.out.println("parent static block");
    }

    public Parent() {
        System.out.println("parent construct");
    }
}
