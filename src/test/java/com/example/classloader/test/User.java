/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.classloader.test;

/**
 * @author hobo
 * @description 用户类
 * @email hobo@bubi.cn
 * @creatTime 2018/2/24 10:02
 * @since 1.0.0
 */
public class User {
    public User(String flag) {
        System.out.println("user:" + flag);
    }
}
