/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.classloader.test;

import org.junit.Test;

/**
 * @author hobo
 * @description 类初始化测试类
 * @email hobo@bubi.cn
 * @creatTime 2018/2/24 10:04
 * @since 1.0.0
 */
public class ClassLoaderTest {
    private User user = new User("classloader test");
    @Test
    public void test(){
        Son son = new Son();
        son.print();
    }
}
