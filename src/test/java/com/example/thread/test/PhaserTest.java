/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.thread.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/2/22 18:17
 * @since 1.0.0
 */
public class PhaserTest {
    private transient String a;

    public static void main(String[] args) {
        List a = new ArrayList();
        a.add(5);
        System.out.println("begining");
        Phaser phaser = new Phaser(3);
        phaser.bulkRegister(1);
        int count = phaser.register();
        System.out.println(count);
        System.out.println( phaser.getRegisteredParties());
        while (!phaser.isTerminated()) {
            phaser.arriveAndAwaitAdvance();
        }
        System.out.println(11111);

    }
}
