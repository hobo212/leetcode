/*
 * Copyright (c) 2017-2022 布比（北京）网络技术有限公司.
 * All rights reserved.
 */

package com.example.thread.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author hobo
 * @description TODO
 * @email hobo@bubi.cn
 * @creatTime 2018/2/22 17:22
 * @since 1.0.0
 */
public class CountDownLatchTest {

    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(5);
        final List<Runnable> subTasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    System.out.println("doing sth begin" +System.nanoTime());
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        countDownLatch.countDown();
                    }
                    System.out.println("doing sth end" +System.nanoTime());
                }
            };
            subTasks.add(runnable);
        }
        System.out.println("this is in main task method begin :" + System.nanoTime());
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        for (Runnable subTask : subTasks) {
            executorService.submit(subTask);
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        System.out.println(countDownLatch.getCount() + "this is in main method :" + System.nanoTime());
    }
}
